## README
---

  > Testing in general refers to the automated debugging in the functionality that various modern programming languages, environment and frameworks provide programmers. Testing your code before deploying or even pushing is a great way to ensure that all the fuzziness, moving parts in your code has been taken care of before your code leaves your nest and flies into the world where it is ridiculed and mocked by other software developers.  
  

There are three types of testing 

-  **Unit Testing :** These types of tests are done to ensure that a single unit or an independent function is working properly and there are no issues with it. There must be a lot of this to ensure that your application works properly and have good code coverage.  
-  **Integration Testing :** These types of tests are done to ensure that a composite functionality or interdependent functionalities related to one major feature is working correctly and there is no domino effect where adding a new feature affects some old feature. There must be some of these.
- **End to End Testing :** These types of tests are built to ensure that the application or a group of abstraction is achieving its goal and no error occurs in the process. There must be very few of these

All the tests above require a certain amount of time to execute hence their number and frequency of testing should be kept in mind in accordance with the cost of execution. 

This repository will provide you some of the [BEST PRACTICES](https://bitbucket.org/GauravRozatkar/testing-doc-nodejs/src/master/best_practices.md) for testing stolen from the best in the biz, some useful links that could point you to articles and information we robbed from for further study. Few tools which the author handpicked and of course a comprehensive guide on [HOW TO FOLLOW TDD](https://bitbucket.org/GauravRozatkar/testing-doc-nodejs/src/master/get_started_with_TDD.md) to do better testing using express, mongodb and jest.    


## 
