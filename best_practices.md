## BEST PRACTICES FOR TESTING 

---
#### ISOLATED :

 Every test should execute independent of each other. The testing data should also be isolated from the rest of the tests. Each test case should have it’s own testing data. It should not fail in between suddenly. A test should be entered and exited with the pass or fail result smoothly. There should not be any dependency, abstraction or indirection in a test.

#### NOMENCLATURE : 

Each test name should include 3 parts so that people reading the test cases know what’s being tested. A test name should include : 



*   what is being tested
*   what scenarios
*   What's the expected result?

#### TEST COVERAGE :

Tests should cover as much source code as possible depending on the nature of the application and type of testing.

#### USE PLUGINS FOR TESTING TOOLS :

Using specific lint plugins for test files is a great way to avoid skipping of tests and getting the correct result after testing.

#### USE MOCKS, STUBS AND SPIES :

Stubs and mocks are both dummy objects for testing, while stubs only implement a pre-programmed response, mocks also pre-program specific expectations. Stubs provide canned answers to calls made during the test, usually not responding at all to anything outside what's programmed in for the test. Spies are stubs that also record some information based on how they were called. One form of this might be an email service that records how many messages it has sent.

#### USE REALISTIC INPUTS : 

Often production bugs are revealed under some very specific and surprising input — the more realistic the test input is, the greater the chances are to catch bugs early. Use dedicated libraries like [Faker](https://www.npmjs.com/package/faker) to generate pseudo-real data that resembles the variety and form of production data

#### USE TAGS OR GROUPS :

Test suites as well as tests themselves can be categorised using arbitrary strings. These categories can then be used to specify particular types of tests you want to run (or tests you want to ignore). For example tests that work on Linux but fail on Windows could have a broken-on-windows category and then CI runs on Windows could ignore those tests.

#### MUTATION TESTING :

Mutation testing is the type of testing in which the test cases' logical condition is tweaked (mutated) to deliberately fail the tests, or if it fails, then to pass it. The logic can also be changed for the same reasons. It is also popularly called as planting a bug. There are various libraries that you can get on the Internet, but one of them, and most popular, is [Stryker](https://stryker-mutator.io/stryker/), which can be used for the same purpose.

#### TDD :

TDD stands for Testing Driven Development it involves three major steps



*   Write Tests
*   Pass Tests
*   Refactor Code

So instead of writing function first write all the tests you expect the function to pass, then run the tests and change code until all the tests are passed once all of your tests pass you can refactor your code to optimize the function and rerun the tests once there’s no more room for further optimization and the tests have all passed you have successfully developed an optimal solution which is not failing. 


### Links you may want to refer :

[https://github.com/goldbergyoni/javascript-testing-best-practices/](https://github.com/goldbergyoni/javascript-testing-best-practices/)

[https://dzone.com/articles/19-best-practices-for-automation-testing-with-node](https://dzone.com/articles/19-best-practices-for-automation-testing-with-node)

[https://medium.com/@me_37286/yoni-goldberg-javascript-nodejs-testing-best-practices-2b98924c9347](https://medium.com/@me_37286/yoni-goldberg-javascript-nodejs-testing-best-practices-2b98924c9347)

[https://martinfowler.com/articles/mocksArentStubs.html](https://martinfowler.com/articles/mocksArentStubs.html)

[https://levelup.gitconnected.com/node-js-best-practices-testing-and-quality-e2257fda95b1](https://levelup.gitconnected.com/node-js-best-practices-testing-and-quality-e2257fda95b1)

[https://medium.com/coding-stones/separating-unit-and-integration-tests-in-jest-f6dd301f399c#:~:text=As%20of%20today%20there%20is,yourgroup%E2%80%9D%20in%20the%20command%20line.](https://medium.com/coding-stones/separating-unit-and-integration-tests-in-jest-f6dd301f399c#:~:text=As%20of%20today%20there%20is,yourgroup%E2%80%9D%20in%20the%20command%20line.)

[https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da](https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da)

[https://dev.to/snird/the-difference-between-mocks-and-stubs-explained-with-js-kkc](https://dev.to/snird/the-difference-between-mocks-and-stubs-explained-with-js-kkc)

[https://martinfThe difference between mocks and stubs, explained with JSowler.com/articles/mocksArentStubs.html](https://martinfowler.com/articles/mocksArentStubs.html)

[https://medium.com/javascript-scene/tdd-changed-my-life-5af0ce099f80](https://medium.com/javascript-scene/tdd-changed-my-life-5af0ce099f80)

[https://devhints.io/](https://devhints.io/)


## 
