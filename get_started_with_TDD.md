## HOW TO FOLLOW TDD
---
Let’s start with a set of features that a banking system may have and peek on how one could get started with test driven development.

 **Feature :**  Simple Interest Calculator
 
**Testing Framework used :** JEST


Now to implement a function that calculates simple interest we first have to look into what kind of outputs a function like it could generate in short we have to start with writing test cases first

A test suit for test cases implementing simple interest functionality might look something like this

```javascript
// interest for valid values of principal rate and time

test('expects to calculate correct interest for valid values', () => {
    expect(controller.calculateInterest(1000, 4, 2)).toBe(80);
});

// if time is not specified it defaults to 1 and result must be calculated for given rate

test('expects to calculate correct interest for one year for valid values of principal and rate', () => {
    expect(controller.calculateInterest(1000, 4)).toBe(40);
    expect(controller.calculateInterest(1000, 4, null)).toBe(40);
});

// if rate is not defined it must be taken as 4.5% and result must be calculated for given time

test('expects to calculate correct interest for valid values of principal and time and take rate as 4.5%', () => {
    expect(controller.calculateInterest(1000, null, 1)).toBe(45);
    expect(controller.calculateInterest(1000, null, null)).toBe(45);
});

//if principal value is null expect an error message of invalid values

test('expects to send back an error message if principle is not present', () => {
    expect(controller.calculateInterest(null, null, 1)).toBe("The values you added are invalid. Please try again with valid values");
    expect(controller.calculateInterest(null, 3, 1)).toBe("The values you added are invalid. Please try again with valid values");
    expect(controller.calculateInterest(null, null, null)).toBe("The values you added are invalid. Please try again with valid values");
});
```
Notice how each test case relates to each and every scenario that could arise. It not only helped us to build a mental map of what should be done but now we would be more confident in coding our solution. It’s important only to give real world values to the function so that it could perform better.

Now let’s start coding our solution

For a simple solution like this


```javascript
const calculateInterest = (principal, rate = 4.5, time = 1) => {
    return (principal * rate * time) / 100;
};
```


We get following output in the console

![alt_text](https://bitbucket.org/GauravRozatkar/testing-doc-nodejs/raw/eb9cbf0cf681053480a26575a316e1ab5661356b/images/Tdd1.jpg "console output for test cases")


Let’s look at all the tests 

![alt_text](https://bitbucket.org/GauravRozatkar/testing-doc-nodejs/raw/eb9cbf0cf681053480a26575a316e1ab5661356b/images/Tdd2.jpg "console output for test cases")


As you can see from the above image just because we used correct naming conventions we are able to identify what could be wrong in our code

Let’s refactor the code such that all tests pass.


```javascript
const calculateInterest = (principal, rate = 4.5, time = 1) => {
    if (!principal)
        return "The values you added are invalid. Please try again with valid values";
    if (!rate)
        rate = 4.5;
    if (!time)
        time = 1;
    return (principal * rate * time) / 100;
};
```


Now let’s run our test again and see the output

Now that we are confident that our tests work and give us expected results we can go ahead and optimize our function further


```javascript
const calculateInterest = (principal, rate, time) => {
    if (!principal)
        return "The values you added are invalid. Please try again with valid values";
    if (!rate)
        rate = 4.5;
    if (!time)
        time = 1;
    return (principal * rate * time) / 100;
};
```


Now let’s run our test again and see the output

![alt_text](https://bitbucket.org/GauravRozatkar/testing-doc-nodejs/raw/6e3b511c1d5c6f7e238e8c029e161113511f73a0/images/Tdd3.PNG.jpg "console output for test cases")


Great, we have now successfully created and tested a function and can be confident about it’s result in future.

#### The key points we learned : 
---
- Isolation of a function might help us in pinpointing error easily.
- A good name for test will allow us to debug an error before even getting into code.
- Writing a test before creating the function ensures maximum code coverage.
- Using realistic inputs help in real world simulation.

 


## 
