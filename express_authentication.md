
### README
---
This repository contains boilerplate code for express application along with jwt authentication it is created completely following the best practices known to author below are the instructions for how the application was created from scratch along with all the [dependencies](#package).

Let’s start with creating a new node application by using NPM

![alt_text](images/express1.jpg "image_tooltip")
 

Now that we have our package.json ready we will need following packages to be installed for our project 

express, mongoose, body-parser, @hapi/joi, jest, bcrypt, jsonwebtoken 

Use  `npm install express mongoose body-parser @hapi/joi jest bcrypt jsonwebtoken`
command to do that and then check in package.json file if all the packages are installed successfully



At this point your package.json should look like this

###### Package
```javascript
{
  "name": "express-auth",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "@hapi/joi": "^17.1.1",
    "bcrypt": "^5.0.0",
    "body-parser": "^1.19.0",
    "express": "^4.17.1",
    "jest": "^26.1.0",
    "jsonwebtoken": "^8.5.1",
    "mongoose": "^5.9.22"
  }
}
```


Now let’s create a index.js file and setup our express app and connect to our db

```javascript
const express = require("express");
const app = express();
const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/auth-db", { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log("connected to Db");
});

app.listen(3000, () => {
    console.log("server started");
});
```


On running above nodejs file we must get following output

![alt_text](images/express2.jpg "image_tooltip")

Now let’s setup our scripts in package.json

```javascript
{
    "name": "express-auth",
    "version": "1.0.0",
    "description": "",
    "main": "index.js",
    "scripts": {
        "start": "node index.js",
        "test": "jest"
    },
    "author": "Gaurav Rozatkar",
    "license": "ISC",
    "dependencies": {
        "@hapi/joi": "^17.1.1",
        "bcrypt": "^5.0.0",
        "body-parser": "^1.19.0",
        "express": "^4.17.1",
        "jest": "^26.1.0",
        "jsonwebtoken": "^8.5.1",
        "mongoose": "^5.9.22"
    }
}
```

Now we should be able to get following output on running npm start

![alt_text](images/express3.jpg "image_tooltip")

This would help us to define scripts for our different test cases in future

Moving ahead let’s now create our project structure and start by creating following folders

routes, model, helpers, tests and controllers

Once we have the folder structure setup let’s now modify our index.js to handle auth routes  

```javascript
const express = require("express");
const app = express();
const authRouter = require("./routes/auth.js");
const mongoose = require('mongoose');
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

mongoose.connect("mongodb://localhost:27017/auth-db", { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log("connected to Db");
});

app.use('/auth', authRouter);

app.listen(3000, () => {
    console.log("server started");
});
```

Let’s create an auth router in routers package inside auth.js file
```javascript
var express = require('express');
var router = express.Router();

router.post('/register', async(req, res) => {
    res.send("Register Route Hit")
});

router.post('/login', async(req, res) => {
    res.send("Login Route Hit")
});

module.exports = router;
```
So now that we have our routers written let’s start the server and test the requests via postman

![alt_text](images/express4.jpg "image_tooltip")

Now that we have all of this verified let’s move the functions to a controller folder in file authController.js and call only the respective function in router in this way we would be able to build our app in an MVC standard which would make it easier for us to debug and work with the app.

```javascript
module.exports = {
    register: async(req, res) => {
        res.send("Register Route Hit");
    },
    login: async(req, res) => {
        res.send("Login Route Hit");
    }

};
```
Now let’s create a models folder and create a new user model with desired schema we will use this model to post, fetch, update and delete users.

```javascript
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const userSchema = new mongoose.Schema({

    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }

});
userSchema.pre('save', async function() {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(this.password, salt);
    this.password = hashedPassword;
});
userSchema.methods.isValidPassword = async function(password) {
    try {
        return await bcrypt.compare(password, this.password);
    } catch (err) {
        return err;
    }
};

module.exports = mongoose.model('user', userSchema);
```

As you can see here we have four fields email, password, createdAt and updatedAt. createdAt and updatedAt are timestamps of type Date. While email and password are required strings with different properties.

Before saving a new user   ` userSchema.pre('save'` is called which hashes the password and saves it into the db. As well as there is a method `.isValidPassword` which checks if the password is equal or not.

Now that our model is ready and all the controllers are ready too we need to start by writing test cases for registration functionality in our tests folder, but before that let’s create helper package in our root directory that could provide helpers to our controllers and tests with miscellaneous operations like signing jwt token.

Let’s start with a joi validation_schema which could help us validate our user object before performing any operation on it to maintain data integrity and consistency.

```javascript
const joi = require("@hapi/joi");

const authSchema = joi.object({
    email: joi.string().email().lowercase().required(),
    password: joi.string().required()
});

module.exports = { authSchema }
```
 Now let’s create jwtHelper.js file for our jwt token related operations
```javascript
const jwt = require("jsonwebtoken");
module.exports = {
    signAccessToken: (userId) => {
        return new Promise((resolve, reject) => {

            const payload = { "userId": userId };
            const secret = "d1d834bd134dd82f49d9b4030287bdb7d3e7f09f6e1f74ec4c1d933ba7eac0f1";
            const options = {};
            jwt.sign(payload, secret, options, (err, token) => {
                if (err) reject(err);
                resolve(token);

            });
        });
    },
    verifyAccessToken: (req, res) => {
        if (!req.headers['Authorization']) return "Authorization Token not found"
        const authHeader = req.headers['Authorization'];
        const bearerToken = authHeader.split(' ');
        const token = bearerToken[1];
        jwt.verify(token, "d1d834bd134dd82f49d9b4030287bdb7d3e7f09f6e1f74ec4c1d933ba7eac0f1", (err, payload) => {
            if (err) return err;
            return payload;
        })

    }

};
```

The function `signAccessToken` is used to create a new jwt token and pass on to the calling method we can send it in our login api response. The function `verifyAccessToken` is used to verify the jwt token came in request it could help us in setting up a proper middleware for authentication. 

Create a new unit folder in the test folder then create a new file named auth.test.js this file would contain all of our tests for authentication functionality.

By this point we have successfully created all the packages and files required by our project 

![alt_text](images/express5.jpg "image_tooltip")


Before starting a test for registration we need to understand the basic concept of test, a test should be isolated and independent of the rest of the functions which means there are two major scenarios we have to look at before writing a test. Is the test we are writing truly isolated? If not how can we make it so?

Before registering a user it’s better to check for validation since we already have used joi to validate the user schema let’s first write a test that could ensure us that the parameters sent by user for registration are indeed valid to do so we need to break down our original registration method into two parts namely 
- ###### Validation
- ###### User Creation

If both of them pass successfully we can be assured that the controller is working as desired.

So let’s start by writing test cases for validation

```javascript
let authController = require("../../controllers/AuthController");

test('expects to respond with bad request error for invalid values', () => {
    let req = {
        body: {
            email: null,
            password: null
        }
    };
    expect(authController.validateRequest(req)).toMatchObject({ status: 400, error: "Bad Request" });
    req.body.email = "apple";
    req.body.password = "password";
    expect(authController.validateRequest(req)).toMatchObject({ status: 400, error: "Bad Request" });
    req.body.email = null;
    req.body.password = "password";
    expect(authController.validateRequest(req)).toMatchObject({ status: 400, error: "Bad Request" });
    req.body.email = "apple@xyz.com";
    req.body.password = null;
    expect(authController.validateRequest(req)).toMatchObject({ status: 400, error: "Bad Request" });

});

test('expects to respond with 200 status for valid values', () => {
    let req = {
        body: {
            email: "apple@xyz.com",
            password: "password"
        }
    };
    expect(authController.validateRequest(req)).toMatchObject({ status: 200, error: null });
});
```
Once we have tried every permutations of scenarios we can be confident that our test will cover everything that’s desired from it

Let’s create a validRequest module in the controller now and add a helper method  `var { authSchema } = require("../helpers/validation_schema"); ` to get our validation correctly

```javascript
var { authSchema } = require("../helpers/validation_schema");

module.exports = {
    register: async(req, res) => {
        res.send("Register Route Hit");
    },
    validateRequest: (req) => {
        const validationResult = authSchema.validate(req.body);
        if (!validationResult.error) return { status: 200, error: null };
        else return { status: 400, error: "Bad Request" };

    },
    login: async(req, res) => {
        res.send("Login Route Hit");
    }

};
```


Once this is done now let’s run our test cases and figure out if this is working or not 

Now let’s refactor the code if needed and rerun the tests to verify nothing is broken

![alt_text](images/express7.jpg "image_tooltip")


Great! All of our tests pass without any error now we can be assured that the user details going in db is completely valid

After confirmation that code is optimised and all tests are passing let’s start working on createUser functionality. Write the test cases for registration of users since we have already validated the requested object. We need not worry about validation and we can go ahead create test cases for the user creation process.

```javascript
test('expects to throw email already exists error for existing user', () => {
    req.body.email = "apple@xyz.com";
    req.body.password = "password";
    expect(authController.createUser(req.body).toMatchObject({ status: 400, error: "User already exists" }));
});

test('expects to create account for non existing user', () => {
    req.body.email = "apple@gmail.com";
    req.body.password = "password";
    expect(authController.createUser(req.body).toMatchObject({ status: 200, error: null, message: "User account successfully created" }));
});
```


Let’s code createUser function now according to the tests we prepared.

```javascript
createUser: async(user) => {
        try {
            let newUser = await User.findOne({ email: user.email });
            if (newUser) return { status: 400, error: "User already exists" };
            await User.create(user, async(err, data) => {
                if (err) return { error: err };
                else
                    return { status: 200, error: null, message: "User account successfully created" };
            });
        } catch (error) { return error; }
    },
```
Since we already have two different functions to carry out a task we can move them to a services folder to make our code more manageable 

```javascript
const User = require("../models/User");
var { authSchema } = require("../helpers/validation_schema");

module.exports = {
    validateRequest: (req) => {
        const validationResult = authSchema.validate(req.body);
        if (!validationResult.error) return { status: 200, error: null };
        else return { status: 400, error: "Bad Request" };

    },
    createUser: async(user) => {
        try {
            let newUser = await User.findOne({ email: user.email });
            if (newUser) return { status: 400, error: "User already exists" };
            await User.create(user, async(err, data) => {
                if (err) return { error: err };
                return { status: 200, message: "User account successfully created" };
            });
        } catch (error) { return { status: 400, error: error }; }
    }
}
```




Your folder structure must look like this now 

![alt_text](images/express8.jpg "image_tooltip")
 
Now let’s change the controller file also to factor in services


```javascript
var { authSchema } = require("../helpers/validation_schema");
const authService = require("../services/AuthService");
module.exports = {
    register: async(req, res) => {
        const validationError = authService.validateRequest(req).error;
        if (!validationError) {
            const responseObj = await authService.createUser(req.body);
            res.send(responseObj);
        } else res.send({ status: 400, error: validationError });
    },
    login: async(req, res) => {
        res.send("Login Route Hit");
    }
};
```

Now let's run the tests again

![alt_text](images/express9.jpg "image_tooltip")

Now let's repeat the same steps for login function. 
Your tests for login must look like this
```javascript
test('expects to throw user does not exist error for non existing user', () => {
    req.body.email = "apple@gmail.abc";
    req.body.password = "password";
    expect(authService.createUser(req.body)).resolves.toMatchObject({ status: 400, message: "User not registered" });

});

test('expects to respond with Password invalid error for wrong password', () => {
    req.body.email = "apple@gmail.com";
    req.body.password = "password123";
    expect(authService.createUser(req.body)).resolves.toMatchObject({ status: 400, message: "Password invalid" });

});


test('expects to respond with correct accesstoken for valid credentials', () => {
    req.body.email = "apple@gmail.com";
    req.body.password = "password";
    expect(authService.createUser(req.body)).resolves.toMatchObject({
        "status": 200,
        "error": null,
        "data": {
            "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZjBhZjQxY2NlZmQxYzI4ODQ2Y2U1OTEiLCJpYXQiOjE1OTQ2MDU1NTB9.ZhqCZ5rrJyQzeW4aFuY_Cs89pG4TAs9iplJomnb-lrg"
        }
    });
```



Your login function in authController must look like this

```javascript
 login: async(req, res) => {
        const validationError = authService.validateRequest(req).error;
        if (!validationError) {
        try {
         const responseObj = await authService.login(req.body);
         res.send(responseObj);
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }}

}
```
Notice that since we already had ```validateRequest(req)``` function created for createUser we can just reuse that function in login function. We are also going to use ```signAccessToken method from jwtHelper``` to get the jwt token for our user. Now your service file must look like this
```javascript
const User = require("../models/User");
const { authSchema } = require("../helpers/validation_schema");
const{signAccessToken,  verifyAccessToken }=require("../helpers/jwtHelper");

module.exports = {
    validateRequest: (req) => {
        const validationResult = authSchema.validate(req.body);
        if (!validationResult.error) return { status: 200, error: null };
        else return { status: 400, error: "Bad Request" };

    },
    createUser: async(user) => {
        try {
            let newUser = await User.findOne({ email: user.email });
            if (newUser) return { status: 400, error: "User already exists" };
            await User.create(user, (err, data) => {
                if(!err) return err;
                return { status: 200,error:null,data:{ message: "User account created successfully." }};
            });
            return { status: 200,error:null,data:{ message: "User account created successfully." }};
        } catch (error) { return { status: 400, error: error }; }
    },
    login: async(user)=>{
        const userInfo = await User.findOne({ "email": user.email });
        if (!userInfo) return( { status: 400, error:"User not registered"});
        const isMatch = await userInfo.isValidPassword(user.password);
        if (!isMatch) return( { status: 400, error: "Password invalid"});
        else {
            const accessToken = await signAccessToken(userInfo.id)
            return( { status: 200, error: null, data:{accessToken: accessToken} });
        }
    }
}
```
Just run all the test cases once again and test the response from postman once and that's it we have successfully created an express application with basic authentication.

![alt_text](images/express12.jpg "image_tooltip")

Great our application seems to be working fine let's see the output on postman now

![alt_text](images/express10.jpg "image_tooltip")


![alt_text](images/express11.jpg "image_tooltip")

The repo for this application is [linked here]() to quickly get started with express.  


